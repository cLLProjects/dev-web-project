loading = true;
MAX_BOMBAS = 15;
qtdPosicoesSemBombas = 81 - MAX_BOMBAS;
posicoesLimpas = 0;
bombasPosicionadas = 0;
array = [];
board = new Map([]);
refresh = false;
flaggedMines = 0;
mapFlaggedMines = new Map([]);
arrayPosicoesReveladas = [];
posicoesRevelar = [];
function putArrayPosicoesReveladas(id){
	if(!arrayPosicoesReveladas.includes(id)){
		arrayPosicoesReveladas[posicoesLimpas] = id;
		posicoesLimpas++;
	}
}

function gerarTabuleiro(){

}
//Função chamada pelo botão "Gerar campo minado!";
function placeRandomBombs(){
	line    = String.fromCharCode(Math.floor(Math.random() * (105 - 97 + 1)) + 97);
	column  = Math.floor(Math.random() * 9) + 1;
	posicao = line + String(column);
	placeBomb(posicao);
	if (bombasPosicionadas < MAX_BOMBAS){
		placeRandomBombs();
	}
	document.getElementById("placeBomb").disabled = true;
	document.getElementById("gameStatus").innerText = "Boa sorte...";
	loadGame();
}

//Função utilizada por placeRandomBombs();
function placeBomb(id){
	if (bombasPosicionadas < MAX_BOMBAS){
		bombaEncontrada = array.includes(id);
		if (!bombaEncontrada){
			//document.getElementById(id).style.backgroundImage = "url('./images/bomb.jpg')";
			array[bombasPosicionadas] = id;
			bombasPosicionadas++;
		}	
	}
}

//Função utilizada por placeRandomBombs(),
// para carregar o tabuleiro em um mapa <board>;
function loadGame(){
	currentPosition = "a1";

	for(i = 0; i < 81; i ++){	
		line        = currentPosition.substring(0,1);//a
		nextLineAsc = line.charCodeAt(0) + 1;//b
		column      = parseInt(currentPosition.substring(1,2));//1  - a1
		if (array.includes(currentPosition)){
			board.set(currentPosition, "bomb");
		}else if(currentPosition == "j1"){
				break;
		}else{
			nearBombs = getNearBombs(currentPosition);
			board.set(currentPosition, nearBombs);
		}
		
		if(column < 9){
			currentPosition = String(line) + String((column + 1));
		}else{
			currentPosition =  String.fromCharCode(nextLineAsc) + "1"; 
		}
	}
}

function getNearBombs(id){
	//EXEMPLO: B2
	nearBombs   = 0;
	firstLine   = id.charCodeAt(0) - 1;//B - 1 = A
	currentLine = firstLine;
	lastLine = firstLine + 2;
	firstColumn = parseInt(id.substring(1,2)) - 1;//2 - 1 = 1
	currentColumn = firstColumn;
	while(currentLine <= lastLine){
		for (j = 0; j < 3; j++){

			posicao = String.fromCharCode(currentLine) + 
						currentColumn;

			if(array.includes(posicao)){
				nearBombs++;
			}
			currentColumn++;
		}
		currentColumn = firstColumn;
		currentLine++;
	}
	return nearBombs;
}

function flag(id){
	if (board.get(id) != "flag"){
		mapFlaggedMines.set(id, board.get(id));
		board.set(id, "flag");
		flaggedMines++;
		document.getElementById(id).style.backgroundImage = "url('./images/safeFlag.png')";
	}else{
		board.set(id, mapFlaggedMines.get(id));
		document.getElementById(id).style.backgroundImage = "";
		document.getElementById(id).style.backgroundColor = "#40E0D0";
	}
}

function game(id){
	gameStatus = document.getElementById("gameStatus").innerText;

	if (gameStatus == "Boa sorte..."){
		acaoJogador(id);
	}else if (gameStatus == "Perdeu" || gameStatus == "Vitória"){
		if(refresh){
			document.location.reload(true);
		}
		refresh = true;
	}
}

function acaoJogador(id){
	bombaEncontrada = array.includes(id);
	if(board.get(id) == "flag"){
		//ignore
	}else if (bombaEncontrada){
		//Bomba neste local...
		lose(id);
		showGame();
	}else if(board.get(id) == 0){
		lookAround(id, true, true);
	}else if(!bombaEncontrada && !arrayPosicoesReveladas.includes(id)){
		showPosition(id);
	}
	console.log(posicoesLimpas);
	if(arrayPosicoesReveladas.length == (81 - MAX_BOMBAS)){
		document.getElementById("gameStatus").innerText = "Vitória";
	}
}

function showPosition(id){
	document.getElementById(id).innerText = board.get(id);	
	document.getElementById(id).style.backgroundColor = "#009900";
	putArrayPosicoesReveladas(id);
	console.log(arrayPosicoesReveladas);
}
//id é formado por uma letra e um número
//letra any-1: numero -1,  numero, numero +1
//letra any  : numero -1,  numero, numero +1
//letra any+1: numero -1,  numero, numero +1
function lookAround(id, showGame, zeroCase){
	//EXEMPLO: B2
	nearBombs   = 0;
		
	firstLine   = id.charCodeAt(0) - 1;//B - 1 = A
	
	currentLine = firstLine;
	lastLine = firstLine + 2;
	firstColumn = parseInt(id.substring(1,2)) - 1;//2 - 1 = 1
	currentColumn = firstColumn;
	while(currentLine <= lastLine){
		for (j = 0; j < 3; j++){

			posicao = String.fromCharCode(currentLine) + 
						currentColumn;

			if(array.includes(posicao)){
				nearBombs++;
			}
			if(!loading){
				if(board.get(posicao) == 0 && showGame){
					showPosition(posicao);
				}
				if(zeroCase && showGame && board.has(posicao) && !arrayPosicoesReveladas.includes(posicao)){
					showPosition(posicao);
				}
				if(posicao != id && board.get(posicao) == 0){
					lookAround(posicao, true);
				}
			}
			currentColumn++;
		}
		currentColumn = firstColumn;
		currentLine++;
	}
	if(showGame && board.get(id) != 0){
		showPosition(id);	
	}
	return nearBombs;
}

function lose(id){	
	board.set(id, "boom");
	document.getElementById(id).style.backgroundColor = "#990000";
	document.getElementById("gameStatus").innerText = "Perdeu";
}

function showGame(){
	for (var [key, value] of board){
		if (value == "bomb"){
			document.getElementById(key).style.backgroundImage = "url('./images/bomb.jpg')";
		}else if(value == "boom"){
			document.getElementById(key).style.backgroundImage = "url('./images/redBomb.png')";
		}else{
			document.getElementById(key).innerText = value;
		}
	}
}

function showGame1(){
	currentPosition = "a1";

	i = 0;
	while (i < 80){
		line        = currentPosition.substring(0,1);//a
		nextLineAsc = line.charCodeAt(0) + 1;//b
		column      = parseInt(currentPosition.substring(1,2));//1  - a1
		if (array.includes(currentPosition)){
			board.set(currentPosition, "bomb");
			document.getElementById(currentPosition).style.backgroundImage = "url('./images/bomb.jpg')";
		}else{
			if(currentPosition == "j1"){
				break;
			}
			lookAround(currentPosition, true);
		}
		
		if(column < 9){
			currentPosition = String(line) + String((column + 1));
		}else{
			currentPosition =  String.fromCharCode(nextLineAsc) + "1"; 
		}
		i++;
	}
}

function hideBomb(position){
	if(position != null){
		document.getElementById(position).style.backgroundImage = "";
	} 
}

